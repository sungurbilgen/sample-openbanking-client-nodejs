/* eslint camelcase:0 */
const {Issuer} = require("openid-client")
const jose = require("node-jose")
const tlsOpts = require("./tls-opts")
const keys = require("./keys/keystore.json")

module.exports = async ({client_id, client_secret, redirect_uri}) => {
  return await jose.JWK.asKeyStore(keys).then(async (keystore) => {
    const nodeIssuer = new Issuer;
    Issuer.defaultHttpOptions = tlsOpts
    return await Issuer.discover(process.env.ISSUER)
      .then(function (nodeIssuer) {

        var method = ''
        if ((nodeIssuer.metadata.token_endpoint_auth_methods_supported == null) ||
          (nodeIssuer.metadata.token_endpoint_auth_methods_supported.includes('client_secret_basic'))) {
          method = 'client_secret_basic'
        } else if (nodeIssuer.metadata.token_endpoint_auth_methods_supported.includes('private_key_jwt')) {
          method = 'private_key_jwt'
        } else if (nodeIssuer.metadata.token_endpoint_auth_methods_supported.includes('tls_client_auth')) {
          method = 'none'
          //'none' is used because the code has already set-up the TLS certificate.
        } else {
          throw new Error('Server does not support authentication method')
        }

        var responseType = ''
        if (nodeIssuer.metadata.response_types_supported.includes('code')) {
          responseType = 'code'
        }
        if (nodeIssuer.metadata.response_types_supported.includes('code id_token')) {
          responseType = 'code id_token'
        }

        return new nodeIssuer.Client(
          {
            client_id,
            client_secret,
            redirect_uri,
            token_endpoint_auth_method: method,
            token_endpoint_auth_signing_alg: 'PS256',
            id_token_signed_response_alg: 'PS256',
            openbankingclient_response_type_to_use: responseType
          },
          keystore,
        )
      })
  })
}
