A sample client for the UK OpenBanking APIs for node.js

Based on various code fragments/help received from Dave Tonge at Moneyhub
(thanks!).

This is a very simple example that only calls the accounts request and accounts
API, and it mainly an example of implementing the security profile correctly.

fintechlabs uses this to test the conformance suite, and others can use it to
see how the conformance suite is used to test relying parties.

See the wiki for instructions:

https://gitlab.com/fintechlabs/fapi-conformance-suite/wikis/Users/How-to-run-OpenBanking-UK-RP-(client)-tests
