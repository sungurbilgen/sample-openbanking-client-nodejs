/* eslint camelcase: 0 */
const config = require("./config.json")
const getClient = require("./client")
const uuid = require("uuid")
const tlsOpts = require("./tls-opts")
const got = require("got")
const R = require("ramda")
const URL = require('url').URL;
const url = require('url');
const jwtDecode = require('jwt-decode');
require('dotenv').load();

// To enable http logging, set NODE_DEBUG environment variable to 'http'

require('https').globalAgent.options.ca = require('ssl-root-cas').create();
require('https').globalAgent.options.rejectUnauthorized = false; // FIXME

const getHeaders = () => ({
  "x-fapi-financial-id": "0015800000jf8aKAAQ",
  // "x-fapi-customer-last-logged-time": new Date(
  //   Date.now() - 30 * 60 * 1000
  // ).toUTCString(),
  // "x-fapi-customer-ip-address": "89.233.114.212",
  // "x-fapi-interaction-id": uuid.v4().substr(0, 24),
})

if (process.env.CONFORMANCE_SERVER === undefined) {
  throw new Error("Environment not set. Need to set variables from config.")
}

const accountRequestUrl = new URL(`${process.env.CONFORMANCE_SERVER}/${process.env.ACCOUNT_REQUEST}`);
const accountsUrl = new URL(`${process.env.CONFORMANCE_SERVER}/${process.env.ACCOUNTS}`);
const testMode = process.env.CLIENTTESTMODE;

const accountRequestData = {
  "Data": {
    "Permissions": [
      "ReadAccountsBasic"
    ]
  },
  "Risk": {}
}

const setupAccountRequest = token => {
  const requestOptions = {
    ...tlsOpts,
    json: true,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json; charset=utf-8",
      "Content-Type": "application/json; charset=utf-8",
      ...getHeaders(),
    },
    body: accountRequestData,
  }
  // console.log(accountRequestData)
  // console.log(requestOptions.headers)
  return got
    .post(accountRequestUrl, requestOptions)
    .then(/*console.log*/)
    .catch(err => {
      console.log("Error from " + accountRequestUrl)
      console.log(err)
      console.log(err.response.body)
    })
}

const callAccountsEndpoint = token => {
  const requestOptions = {
    ...tlsOpts,
    json: true,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json; charset=utf-8",
      "Content-Type": "application/json; charset=utf-8",
      ...getHeaders(),
    }
  }
  // console.log(requestOptions.headers)
  return got
    .post(accountsUrl, requestOptions)
    .then(/*console.log*/)
    .catch(err => {
      console.log("Error from " + accountsUrl)
      console.log(err)
      console.log(err.response.body)
    })
}

var exp = Date.now() + (300 * 1000);
exp = Math.floor(exp / 1000);

const getAuthUrl = ({client, id, state, nonce, response_type}) => {
  const claims = {
    id_token: {
      openbanking_intent_id: {
        value: id,
        essential: true,
      },
      acr: {
        essential: true,
        values: ["urn:openbanking:psd2:sca", "urn:openbanking:psd2:ca"],
      },
    },
  }

  const authParams = {
    client_id: config.client_id,
    state,
    scope: "openid accounts",
    nonce,
    response_type,
    redirect_uri: config.redirect_uri,
  }

  return client
    .requestObject(
      R.merge(authParams, {
        claims, exp,
      }),
      {
        sign: "PS256",
      }
    )
    .then(R.assoc("request", R.__, authParams))
    .then(client.authorizationUrl.bind(client))
}

async function authorize(...params) {
  const {pathname, query} = url.parse(params[0], true);
  console.log('authentication request to', pathname);
  console.log('authentication request parameters', JSON.stringify(query, null, 4));
  const response = await got(...params);
  const {query: callback} = url.parse(response.headers.location.replace('#', '?'), true);
  console.log('authentication response', JSON.stringify(callback, null, 4));
  return response;
}

async function authorizationCallback(client, ...params) {
  try {
    const res = await client.authorizationCallback(...params);
    console.log('authentication callback succeeded', JSON.stringify(res, null, 4));
    return res;
  } catch (err) {
    console.log('authentication callback failed', err);
    throw err;
  }
}

const main = async () => {
  try {
    const client = await getClient(config)
    // allow 60 seconds clock skew between our host and the server to
    // account for possible small differences in device clocks
    client.CLOCK_TOLERANCE = 60
    console.log(client)
    let authUrl = ''

    const response_type = client.openbankingclient_response_type_to_use
    const state = "test-state"
    const nonce = "123abcdef"

    if (testMode === "fapi-ob") {
      const token = await client.grant({
        grant_type: "client_credentials",
        scope: "accounts",
      })

      console.log("got client credentials access token: " + token.access_token)

      const data = await setupAccountRequest(token.access_token)

      accountRequestId = data.body['Data']['AccountRequestId']
      console.log("accountRequestId = " + accountRequestId)

      authUrl = await getAuthUrl({
        client,
        state,
        nonce,
        response_type,
        id: accountRequestId,
      })
    } else {
      authUrl = await getAuthUrl({
        client,
        state,
        nonce,
        response_type
      })
    }

    console.log("authorization url is: " + authUrl)

    const authorization = await authorize(authUrl, {followRedirect: false});

    const urlObject = url.parse(authorization.headers.location)
    if (client.openbankingclient_response_type_to_use === 'code') {
      var params = client.callbackParams(authorization.headers.location)
    } else if (client.openbankingclient_response_type_to_use === 'code id_token') {
      var hash = urlObject.hash
      if (hash != null) {
        if (hash.startsWith('#')) {
          var new_url = '';
          new_url = "?" + hash.substr(1);
          params = client.callbackParams(new_url);
          const encodedIdToken = params["id_token"];
          const decodedIdToken = jwtDecode(encodedIdToken);

          const receivedIat = decodedIdToken["iat"];
          const clientIat = Math.ceil(Date.now() / 1000);
          const timeDiff = clientIat - receivedIat;
          //https://openid.net/specs/openid-connect-core-1_0.html#IDTokenValidation
          //Section 3.1.3.7-10
          //The iat Claim can be used to reject tokens that were issued too far away from the current time,
          //limiting the amount of time that nonces need to be stored to prevent attacks.
          //The acceptable range is Client specific. So we have decided to allow for 5 minutes.
          const tolerance = client.CLOCK_TOLERANCE  + 5 * 60
          if (timeDiff > tolerance) {
            throw new Error("iat is too far in the past")
          }

          if(!decodedIdToken["s_hash"]){
            throw new Error("s_hash is missing from the the id_token from the authorization endpoint")
          }

          const openbankingIntentId = decodedIdToken["openbanking_intent_id"];
          if (testMode === "fapi-ob") {
            if (openbankingIntentId !== accountRequestId) {
              throw new Error("openbanking_intent_id returned in id_token from authorization endpoint does not match the value sent in the request object")
            }
          }
        } else {
          throw new Error('Malformed fragment (should start with a #)')
        }
      } else {
        throw new Error('Url does not contain fragment')
      }
    } else {
      throw new Error('Server does not support response_type ')
    }

    const tokens = await authorizationCallback(client, config.redirect_uri, params, {response_type, state, nonce});

    const access_token = tokens.access_token

    console.log("access token is: " + access_token)

    const response = await callAccountsEndpoint(access_token);

    console.log("COMPLETE: accounts endpoints response is: " + JSON.stringify(response.body))

  } catch (e) {
    console.log(e)
  }
}

main()
